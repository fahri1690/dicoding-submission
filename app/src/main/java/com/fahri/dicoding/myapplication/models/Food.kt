package com.fahri.dicoding.myapplication.models

data class Food(
    var name: String = "",
    var about: String = "",
    var photo: String = "",
    var origin: String = ""
)