package com.fahri.dicoding.myapplication.fragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.fahri.dicoding.myapplication.R
import com.fahri.dicoding.myapplication.`object`.FoodLists
import com.fahri.dicoding.myapplication.adapter.FoodAdapter
import com.fahri.dicoding.myapplication.models.Food
import com.fahri.dicoding.myapplication.pages.DetailActivity

class HomeFragment: Fragment() {


    private lateinit var rvFoods: RecyclerView
    private var list: ArrayList<Food> = arrayListOf()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view: View = inflater.inflate(R.layout.fragment_home, container, false)
        rvFoods = view.findViewById(R.id.rv_food)
        rvFoods.setHasFixedSize(true)

        list.addAll(FoodLists.listData)
        showRecyclerList()

        return view
    }

    companion object {
        fun newInstance(): HomeFragment{
            val fragment = HomeFragment()
            val args = Bundle()
            fragment.arguments = args
            return fragment

        }
    }

    private fun showRecyclerList() {
        rvFoods.layoutManager = LinearLayoutManager(context)
        val food = FoodAdapter(list)
        rvFoods.adapter = food

        food.setOnItemClickCallback(object : FoodAdapter.OnItemClickCallback {
            override fun onItemClicked(data: Food) {
                showFoodDetail(data)
            }
        })
    }


    private fun showFoodDetail(food: Food) {
        val intent = Intent(activity, DetailActivity::class.java)
        intent.putExtra("name", food.name)
        intent.putExtra("about", food.about)
        intent.putExtra("photo", food.photo)
        intent.putExtra("origin", food.origin)
        startActivity(intent)
    }
}