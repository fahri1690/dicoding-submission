package com.fahri.dicoding.myapplication.pages

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.fahri.dicoding.myapplication.R
import kotlinx.android.synthetic.main.fragment_detail.*

class DetailActivity: AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
                super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_detail)
        getFoodDetails()
    }

    private fun getFoodDetails() {
        val name = intent.getStringExtra("name")
        val from = intent.getStringExtra("about")
        val image = intent.getStringExtra("photo")
        val origins = intent.getStringExtra("origin")

        food_name.text = name
        food_description.text = from
        origin.text = origins

        Glide.with(applicationContext)
            .load(image)
            .apply(RequestOptions().override(250))
            .into(food_image)
    }

}