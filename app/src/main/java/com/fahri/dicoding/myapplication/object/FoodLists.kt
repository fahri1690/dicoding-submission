package com.fahri.dicoding.myapplication.`object`

import com.fahri.dicoding.myapplication.models.Food

object FoodLists {
    private val data = arrayOf(
        arrayOf(
            "Spagghetti Bolognesse",
            "Salah satu kuliner tradisional asal italia yang disajikan dengan 'tagliatelle'. Sangat populer di seluruh dunia karena murah, mudah dibuat, bergizi, dan serbaguna. Tagliatelle merupakan saus daging terkenal yang berasal dari Bologna yang dilumuri diatas spaghetti. Tiga bahan utama spaghetti adalah telur, air, dan tepung. Penasaran sama rasanya? Yuk cobain kuliner yang satu ini.",
            "https://www.bbcgoodfood.com/sites/default/files/styles/recipe/public/recipe/recipe-image/2017/09/spaghetti-bolognese.jpg?itok=KzT6QRKe",
            "Italian Food"
        ),
        arrayOf(
            "Confit de canard",
            "Confit de canard adalah hidangan bebek Prancis yang lezat dan dianggap sebagai salah satu hidangan terbaik di Perancis. Daging disiapkan secara khusus menggunakan proses pengawetan dan proses memasak lambat selama berabad-abad, di mana daging bebek direndam dalam garam, bawang putih, dan thyme hingga 36 jam dan kemudian dimasak perlahan dalam lemaknya sendiri pada suhu rendah ( alternatif untuk menggoreng). Biasanya disajikan dengan kentang panggang dan bawang putih di sampingnya. Saat ini hidangan Prancis ini disajikan di seluruh Prancis, meskipun dianggap sebagai spesialisasi wilayah Gascony.",
            "https://www.expatica.com/media/upload/701331.jpg",
            "French Food"
        ),
        arrayOf(
            "Carbonara",
            "Carbonara merupakan masakan khas italia berupa spagetti yang dimasak bersama saus carbonara bercampur sama potongan daging, keju, telur, garam, dan minyak zaitun. Resep makanan ini termasuk yang mudah dicari dan cukup gampang untuk dipraktikkan di rumah.",
            "https://cdn.idntimes.com/content-images/community/2018/02/172132-640x428-e3fef3373bf4bf5082be98da3bd826e5.jpg",
            "Italian Food"
        ),
        arrayOf(
            "Lasagna",
            "Lasagna merupakan makanan tradisional yang sangat terkenal di Italia. Makanan ini terbuat dari pasta berlapis berisi daging, saus bolognese, keju mozarella, sayuran dan bisa juga makanan lain sesuai selera. Lalu disusun dan dipanggang ke dalam oven.\n" +
                    "\n" +
                    "Dengan perpaduan rasa dari berbagai bahan makanan, lasagna menjadi makanan yang kaya cita rasa sekaligus lezat dan nikmat, yang akan membuat siapa saja yang melihat makanan ini bakal ngiler.\n" +
                    "\n",
            "https://cdn.idntimes.com/content-images/community/2018/02/lasagna-formaggioashx-9f321679b9a816c032b0dc9ccfd30777.jpg",
            "Italian Food"
        ),
        arrayOf(
            "Beef Burguignon",
            "Makanan ini sejenis dengan rendang yang berasal dari Sumatera Barat, Indonesia. Beef burguignon disebut-sebut sebagai hidangan paling lezat di Kota Mode. Bahkan, banyak yang rela mengantri panjang demi mendapatkannya. Terbuat dari daging sapi yang mempunyai tekstur lembut.\n" +
                    "\n" +
                    "Bukan hanya itu, ia juga direbus dalam anggur merah, sehingga cita rasanya lebih khas dan mewah.\n" +
                    "Bumbu yang digunakan, yaitu bawang putih, bawang merah, jamur dan aneka rempah segar.\n" +
                    "\n",
            "https://www.gotravelly.com/blog/wp-content/uploads/2018/05/beef-burguignon.jpeg",
            "French Food"
        ),
        arrayOf(
            "Ratatouille",
            "Kuliner tersebut berasal dari daerah Nice, sehingga kerap disebut sebagai ratatouille nicoise. Terdiri atas bermacam-macam sayuran, seperti bay leave, thyme dan dedaunan lainnya. Umumnya, sajian ini disantap bersama nasi, roti atau kentang. Tapi, sekarang sering dijadikan saus untuk pasta, dicampur dengan omelet dan masih banyak lagi.\n" +
                    "\n" +
                    "Dulu, ratatouille merupakan makanan para petani miskin yang dihidangkan pada musim panas. Isinya berupa courgette atau zucchini (sejenis mentimun), tomat, bawang putih, cabai merah dan cabai hijau.\n" +
                    "\n",
            "https://www.gotravelly.com/blog/wp-content/uploads/2018/05/ratatouille.jpg",
            "French Food"
        ),
        arrayOf(
            "Coq au Vin",
            "Jika diterjemahkan dari Bahasa Perancis, “Coq au Vin” berarti “ayam jantan di wine”. Pada dasarnya, kuliner ini berupa kaki ayam yang dimasak dengan anggur merah dalam waktu lama. Jadi, tekstur dagingnya lebih lembut dan bumbunya pun meresap. Beberapa wilayah di negara tersebut biasanya menggunakan wine lokal sebagai campuran.",
            "https://www.gotravelly.com/blog/wp-content/uploads/2018/05/coq-au-vin-khas-perancis.jpg",
            "French Food"
        ),
        arrayOf(
            "Ayam Betutu",
            "Menggunakan teknik olahan yang unik, Ayam betutu berbahan satu ayam utuh dibubuhi bumbu spesial lalu dibakar. Setelah masak, daging ayam benar-benar lembut dan kaya akan cita rasa. Makanan khas Pulau Dewata ini membuat wisatawan luar dan dalam negeri buru-buru ingin melahapnya.",
            "https://cdn2.boombastis.com/wp-content/uploads/2017/07/2-17.jpg",
            "Indonesian Food"
        ),
        arrayOf(
            "Papeda Irian Jaya",
            "Berbeda dengan beberapa masakan tradisional yang mengandung kolesterol cukup tinggi, Papeda terbuat dari bubur sagu. Meski rasanya tawar, penyajian papeda dengan ikan tongkol berbumbu kunyit adalah hidangan sangat lezat. Selain itu guys, masakan ini juga masuk kategori rendah kolesterol dan mengandung serat dan nutrisi tinggi. Bisa nih jadi pilihan kamu penggila makanan sehat.",
            "https://cdn2.boombastis.com/wp-content/uploads/2017/07/3-16.jpg",
            "Indonesian Food"
        ),
        arrayOf(
            "Rendang Padang",
            "Makanan berbahan dasar daging dan santan kelapa ini memang sangat lezat. Bumbu masakan ini pun hanya bisa lengkap jika dibuat di Indonesia. Meski ada beberapa restoran yang menyajikan masakan ini di luar negeri, tetap saja masih kalah dengan cita rasa asli khas tanah air.",
            "https://cdn2.boombastis.com/wp-content/uploads/2017/07/4-16.jpg",
            "Indonesian Food"
        )
    )

    val listData: ArrayList<Food>
        get() {
            val list = arrayListOf<Food>()
            for (aData in data) {
                val food = Food()
                food.name = aData[0]
                food.about = aData[1]
                food.photo = aData[2]
                food.origin = aData[3]
                list.add(food)
            }
            return list
        }
}